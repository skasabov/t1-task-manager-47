package ru.t1.skasabov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.model.IService;
import ru.t1.skasabov.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
