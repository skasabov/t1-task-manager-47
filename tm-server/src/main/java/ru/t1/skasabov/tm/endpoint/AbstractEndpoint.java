package ru.t1.skasabov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.dto.request.AbstractUserRequest;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.user.PermissionException;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new PermissionException();
        if (role == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
