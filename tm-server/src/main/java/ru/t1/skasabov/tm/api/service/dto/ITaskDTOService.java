package ru.t1.skasabov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll(@Nullable Sort sortType);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sortType);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO create(@Nullable String userId,
                   @Nullable String name, @Nullable String description,
                   @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String projectId);

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
