package ru.t1.skasabov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.dto.IProjectDTOService;
import ru.t1.skasabov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.skasabov.tm.api.service.dto.ITaskDTOService;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.skasabov.tm.exception.field.TaskIdEmptyException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;

public final class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final ITaskDTOService taskService;

    public ProjectTaskDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectDTOService projectService,
            @NotNull final ITaskDTOService taskService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return task;
    }

}
