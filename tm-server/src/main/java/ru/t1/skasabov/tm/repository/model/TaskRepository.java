package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.model.ITaskRepository;
import ru.t1.skasabov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT t FROM Task t", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM Task p WHERE p.id = :id", Task.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM Task t", Task.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM Task t", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task t")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.user.id = :userId AND t.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByCreated() {
        return entityManager.createQuery("SELECT t FROM Task t ORDER BY t.created",
                        Task.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByStatus() {
        return entityManager.createQuery("SELECT t FROM Task t ORDER BY t.status",
                        Task.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByName() {
        return entityManager.createQuery("SELECT t FROM Task t ORDER BY t.name",
                        Task.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.created",
                        Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.status",
                        Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.name",
                        Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId",
                        Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.project.id = :projectId",
                        Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
