package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.model.ISessionRepository;
import ru.t1.skasabov.tm.api.repository.model.IUserRepository;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.SessionRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOne;

    @NotNull
    private static User userTwo;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    protected EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.removeAll();
        userOne = new User();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        userTwo = new User();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUser(userOne);
            else session.setUser(userTwo);
            sessionRepository.add(session);
        }
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUser(userTwo);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 4;
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userOne);
            actualSessions.add(session);
        }
        sessionRepository.addAll(actualSessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userTwo);
            actualSessions.add(session);
        }
        sessionRepository.set(actualSessions);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        sessionRepository.removeAll();
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        sessionRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Session> sessionList = sessionRepository.findAll(USER_ID_TWO);
        sessionRepository.removeAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionList = sessionRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneById(sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Session session = sessionRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findOneById(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test
    public void testFindByIdSessionNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Session session = sessionRepository.findAll().get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test
    public void testFindByIndexSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(0));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Session session = sessionRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test
    public void testFindByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(USER_ID_ONE, 0));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUser(userOne);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Session session = new Session();
        session.setUser(userTwo);
        sessionRepository.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(invalidId));
        Assert.assertTrue(sessionRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final Session session = sessionRepository.findAll(USER_ID_ONE).get(0);
        sessionRepository.removeOne(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - 1;
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        sessionRepository.removeOneById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        sessionRepository.removeOneById(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - 1;
        sessionRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_TWO) - 1;
        sessionRepository.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_TWO));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
